module.exports = {
    name: "99",
    description: "i'm so sorry, please don't use this",
    execute(message, args) {
        var n = 99;
        while (n > 0) {
            if (n != 1) {
                message.channel.send("```" + n + " bottles of beer on the wall. " + n + " bottles of beer."
                + " Take one down, pass it around. " + (n - 1) + " bottles of beer on the wall.```", {
                    tts: true
                });
            } else {
                message.channel.send("```" + n + " bottle of beer on the wall. " + n + " bottle of beer."
                + " Take one down, pass it around. No more bottles of beer on the wall!```", {
                    tts: true
                });
            }
            n -= 1;
        }
    }
}
