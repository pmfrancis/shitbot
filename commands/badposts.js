const { badposts } = require("../json/badposts.json");

module.exports = {
    name: "bestofweb",
    aliases: ["web"],
    description: "Returns something from the collection. Add a number after the command for a specific post.",
    usage: "[post number]",
    execute(message, args) {
        if (!args[0]) {
            var idx = Math.floor(Math.random() * badposts.length);
        } else if (args[0] > 0 && args[0] <= badposts.length) {
            var idx = (args[0] - 1);
        } else {
            message.reply("Please enter a number between 1 and " + badposts.length);
            return;
        }
        var string = badposts[idx];
        
        const maxLength = 180;
        var i = 0;
        var j = maxLength - 1;

        message.channel.send("**#" + (idx + 1) + ":**");

        while (string.length - i > maxLength) {
            if (string.charAt(j) == ' ') {
                message.channel.send("```" + string.slice(i,j) + "```", { tts: true });
                j++;
                i = j;
                j = j + maxLength;
            } else {
                j--;
            }
        }
        message.channel.send("```" + string.slice(i,j) + "```", { tts: true });
    }
}