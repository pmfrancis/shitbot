module.exports = {
    name: "badtts",
    description: "It does tts, but badly",
    execute(message) {
        function doIt() {
            var text = "";
            var possible = "aaaaaabcdeeeeeefghiiiiiijklmnoooooopqrstuuuuuuvwxyz   ";
            var rdm = Math.floor(Math.random() * (55 - 7) + 7);
            for (var i = 0; i < rdm; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            return text;
        }

        message.channel.send(doIt(), {
            tts: true
        });
    }
}