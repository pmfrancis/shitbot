module.exports = {
    name: "between",
    description: "returns a random number between two given numbers",
    execute(message, args) {
        let min = Math.ceil(args[0]);
        let max = Math.floor(args[1]);
        var rdm = Math.floor(Math.random() * (max - min + 1) + min);
        message.channel.send("your number between " + min + " and " + max + " is: **" + rdm + "**");
    }
}
