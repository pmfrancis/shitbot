module.exports = {
    name: "choose",
    description: "randomly chooses based on arguments",
    execute(message, args) {
        let num = args.length;
        let max = Math.floor(num);
        var rdmIdx = Math.floor(Math.random() * num);
        var result = args[rdmIdx];
        message.channel.send("Go with: **" + result + "**");
    }
}