const { cog } = require("../json/cog.json");

module.exports = {
    name: "cog",
    description: "returns random Classics of Game, or a specific video if a number is added after the command, e.g. `!cog 40`",
    usage: "[video number]",
    execute(message, args) {
        if (!args[0]) {
            var rdmIdx = Math.floor(Math.random() * cog.length);
            message.channel.send("https://youtube.com/watch?v=" + cog[rdmIdx] + "?rel=0");
        } else {
            var idx = (args[0] - 1);
            if (idx <= cog.length && idx > 0) {
                message.channel.send("https://youtube.com/watch?v=" + cog[idx] + "?rel=0");
            } else {
                message.channel.send("please choose a number between 1 and " + cog.length);
            }
        }
    }
}