const frinkiac = require("frinkiac");

module.exports = {
    name: "frinkiac",
    aliases: ["f"],
    description: "returns frinkiac images",
    execute(message, args) {
        if (args[0] === "--first") {
            var s = args.slice(1).join(" ");
            var first = true;
        } else {
            var s = args.slice(0).join(" ");
            var first = false;
        }
        frinkiac.search(s)
            .then(function(res) {
                if (res.status !== 200) {
                    throw res;
                } else {
                    return res.data;
                }
            })
            .catch(function(err) {
                throw err;
            })
            .then(function(data) {
                var memeURLs = data.map(frinkiac.memeMap, frinkiac);
                if (memeURLs.length === 0) {
                    message.channel.send("No results found for " + s);
                } else if (first) {
                    message.channel.send(memeURLs[0]);
                } else {
                    var rdmIdx = Math.floor(Math.random() * memeURLs.length);
                    message.channel.send(memeURLs[rdmIdx]);
                }
            });
    }
}
