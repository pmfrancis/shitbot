const Discord = require("discord.js");
const { prefix } = require('../config.json');

module.exports = {
	name: "help",
	description: "List all of my commands or info about a specific command.",
	aliases: ["commands", "h"],
	usage: "[command name]",
	execute(message, args) {
		const data = [];
        const { commands } = message.client;
        
        const embed = new Discord.RichEmbed()
            .setColor("#0099ff")
            .setTitle("Command List");

		if (!args.length) {
			data.push("**Command List:**");
			data.push(commands.map(command => "`" + command.name + "` - " + command.description).join('\n'));
			data.push(`\nYou can send \`${prefix}help [command name]\` to get info on a specific command!`);

			return message.channel.send(data, { split: true })
				.catch(error => {
					console.error("Hey, something isn't working!\n", error);
					message.reply("Something is going bad!")
                });

            // for (command in commands) {
            //     embed.addField(command.name, command.description);
            // }
            // return message.channel.send(embed, { split: true })
            //     .catch(error => {
            //         console.error("Hey, something isn't working!\n", error);
            //         message.reply("Something is going bad!")
            //     });  
		}

		const name = args[0].toLowerCase();
		const command = commands.get(name) || commands.find(c => c.aliases && c.aliases.includes(name));

		if (!command) {
			return message.reply('that\'s not a valid command!');
		}

		data.push(`**Name:** ${command.name}`);

		if (command.aliases) data.push(`**Aliases:** ${command.aliases.join(', ')}`);
		if (command.description) data.push(`**Description:** ${command.description}`);
		if (command.usage) data.push(`**Usage:** ${prefix}${command.name} ${command.usage}`);

		message.channel.send(data, { split: true });
	},
};