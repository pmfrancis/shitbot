const { names } = require("../json/names.json");

module.exports = {
    name: "name",
    aliases: ["nameoftheyear", "n"],
    description: "Returns a random name from Deadspin's Name Of The Year brackets. Add number after command to retrieve specific name, if you know it. Yes they're all real names.",
    usage: "[integer]",
    execute(message, args) {
        if (!args[0]) {
            var idx = Math.floor(Math.random() * names.length);
        } else if (args[0] > 0 && args[0] <= names.length) {
            var idx = (args[0] - 1);
        } else {
            message.reply("Please enter a number between 1 and " + names.length);
            return;
        }
        var string = names[idx];
        
        message.channel.send(string, { tts: true });
    }
}
