const { miramar, erangel, vikendi, sanhok } = require("../json/pubg.json");

module.exports = {
    name: "pubg",
    description: "Chooses a PUBG location to drop",
    execute(message, args) {
        let loc = args[0];
        if (loc === "sanhok") {
            var rdm = Math.floor(Math.random() * sanhok.length);
            message.reply("you are dropping in beautiful \:skull:**" + sanhok[rdm] + "**! \:skull:");
        } else if (loc === "miramar") {
            var rdm = Math.floor(Math.random() * miramar.length);
            message.reply("you are dropping in beautiful \:skull:**" + miramar[rdm] + "**! \:skull:");
        } else if (loc === "erangel") {
            var rdm = Math.floor(Math.random() * erangel.length);
            message.reply("you are dropping in beautiful \:skull:**" + erangel[rdm] + "**! \:skull:");
        } else if (loc === "vikendi") {
            var rdm = Math.floor(Math.random() * vikendi.length);
            message.reply("you are dropping in beautiful \:skull:**" + vikendi[rdm] + "**! \:skull:");
        } else {
            message.reply(loc + " is not a map!");
        }
    }
}
