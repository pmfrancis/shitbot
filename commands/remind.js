module.exports = {
    name: "remind",
    description: "give the user a reminder after a given time",
    usage: "[integer in s] [message]",
    execute(message, args) {
        var wait = args[0] * 1000;
        var str = args.slice(1).join(" ");
        setTimeout(function() {
            message.reply(str);
        }, wait);
    }
}