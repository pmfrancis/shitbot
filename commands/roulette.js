function Roulette() {
    this.dead = false;
    this.i = 0;
    this.chamber = [0,0,0,0,0,0];
}

function pull(game) {
    if (game.chamber[game.i] == 1) {
        game.dead = true;
    } else {
        game.i++;
    }
}

function initialize(game) {
    game.dead = false;
    game.i = 0;
    game.chamber = [0,0,0,0,0,0];
    var bulletPlacement = Math.floor(Math.random() * game.chamber.length);
    game.chamber[bulletPlacement] = 1;
}

var game = new Roulette();

module.exports = {
    name: "roulette",
    description: "it's russian roulette, folks!",
    execute(message, args) {
        if (args[0] === "start" || args[0] === "reset") {
            initialize(game);
            message.channel.send("A single bullet is placed in the chamber");
        }
        if (args[0] === "pull") {
            pull(game);
            if (game.dead == true) {
                message.channel.send("Whoops, you are dead.");
            } else {
                message.channel.send("The hammer releases but no bullet is fired");
            }
        }
    }
}