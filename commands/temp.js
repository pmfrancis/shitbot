function convToF(temp) {
    var num = (temp * (9/5) + 32);
    num = +num.toFixed(3);
    return num;
}

function convToC(temp) {
    var num = (temp - 32) * (5/9);
    num = +num.toFixed(3);
    return num;
}

module.exports = {
    name: "temp",
    description: "converts temps",
    execute(message, args) {
        let temperature = args[0];
        message.react("🇨")
            .then(() => message.react("🇫"));

        const filter = (reaction, user) => {
            return ["🇨", "🇫"].includes(reaction.emoji.name) && user.id === message.author.id;
        };
        
        message.awaitReactions(filter, { max: 1, time: 30000, errors: ['time'] })
            .then(collected => {
                const reaction = collected.first();

                if (reaction.emoji.name === "🇨") {
                    message.channel.send(temperature + "°F is equal to " + convToC(temperature) + "°C!");
                } else {
                    message.channel.send(temperature + "°C is equal to " + convToF(temperature) + "°F!");
                }
            })
        .catch(collected => {
            message.reply("You didn't reply");
        });
    }
}
