module.exports = {
    name: "vowel",
    description: "replaces the vowels with random ones",
    execute(message, args) {
        function vowelSwap(text) {
            var vowels = "aeiou";
            return text.replace(/[aeiou]/gi, function(vowel) {
                return vowels.charAt(Math.floor(Math.random() * vowels.length));
            });
        }

        var str = args.slice(0).join(" ");
        message.channel.send(vowelSwap(str), {
            tts: true
        });
    }
}
