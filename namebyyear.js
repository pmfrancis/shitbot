const { names } = require("../json/namebyyear.json");

module.exports = {
    name: "nametest",
    aliases: ["nnn"],
    description: "Updated version of !name. They're all real names, probably.",
    usage: "[integer]",
    execute(message, args) {
        if (!args[0]) {
            var idx = Math.floor(Math.random() * names.length);
        } else if (args[0] > 0 && args[0] <= names.length) {
            var idx = (args[0] - 1);
        } else {
            message.reply("Please enter a number between 1 and " + names.length);
            return;
        }
        //var string = names[idx];
        
        message.channel.send(idx, { tts: true });
    }
}
