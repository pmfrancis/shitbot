const Discord = require("discord.js");
const Discogs = require("disconnect").Client;
const client = new Discord.Client();
const config = require("./config.json");
const pubg = require("./pubg.json");
const cog = require("./cog.json");
const frinkiac = require("frinkiac");

client.on("ready", () => {
    client.user.setPresence({
        game: {
            name: 'with my ass',
            type: 'PLAYING'
        },
        status: 'available'
    })
})

client.on("message", (message) => {
    if (message.author.bot) return;
    if (message.content.indexOf(config.prefix) !== 0) return;

    //defining args
    const args = message.content.slice(config.prefix.length).trim().split(/ +/g);
    const command = args.shift().toLowerCase();

    // PUBG Drop picker
    if (command === "pubg") {
        let loc = args[0];
        if (loc === "sanhok") {
            var rdm = Math.floor(Math.random() * pubg.sanhok.length);
            message.reply("you are dropping in beautiful \:skull:**" + pubg.sanhok[rdm] + "**! \:skull:");
        } else if (loc === "erangel") {
            var rdm = Math.floor(Math.random() * pubg.erangel.length);
            message.reply("you are dropping in beautiful \:skull:**" + pubg.erangel[rdm] + "**! \:skull:");
        } else if (loc === "miramar") {
            var rdm = Math.floor(Math.random() * pubg.miramar.length);
            message.reply("you are dropping in beautiful \:skull:**" + pubg.miramar[rdm] + "**! \:skull:");
        } else {
            message.reply(loc + " is not a map!");
        }
    }

    // Random tts message
    if (command === "badtts") {
        function doIt() {
            var text = "";
            var possible = "aaaaaabcdeeeeeefghiiiiiijklmnoooooopqrstuuuuuuvwxyz   ";
            var rdm = Math.floor(Math.random() * (55 - 7) + 7);
            for (var i = 0; i < rdm; i++)
                text += possible.charAt(Math.floor(Math.random() * possible.length));

            return text;
        }

        message.channel.send(doIt(), {
            tts: true
        });
    }

    // Tts message with every vowel randomized
    if (command === "vowel") {
        function vowelSwap(text) {
            var vowels = "aeiou";
            return text.replace(/[aeiou]/gi, function(vowel) {
                return vowels.charAt(Math.floor(Math.random() * vowels.length));
            });
        }

        var str = args.slice(0).join(" ");
        message.channel.send(vowelSwap(str), {
            tts: true
        });
    }

    // Tts message with p/b swap, n/m swap
    if (command === "pbnm") {
        function pbnmSwap(text) {
            var newtext = text.replace(/[a-z]/gi, function(pbnm) {
                 switch (pbnm) {
                     case 'p': return 'b';
                     case 'P': return 'B';
                     case 'b': return 'p';
                     case 'B': return 'P';
                     case 'n': return 'm';
                     case 'N': return 'M';
                     case 'm': return 'n';
                     case 'M': return 'N';
                     default: return pbnm.charAt(0);
                 }
            });
            return newtext;
        }

        var str = args.slice(0).join(" ");
        message.channel.send(pbnmSwap(str), {
            tts: true
        });
    }

    // Random number
    if (command === "numberbetween") {
        let min = Math.ceil(args[0]);
        let max = Math.floor(args[1]);
        var rdm = Math.floor(Math.random() * (max - min + 1) + min);
        message.channel.send("your number between " + min + " and " + max + " is: **" + rdm + "**");
    }

    // Coin flip
    if (command === "cointoss") {
        var rdm = Math.floor(Math.random() * 2);
        if (rdm === 0) {
            message.channel.send("**heads!**");
        } else if (rdm === 1) {
            message.channel.send("**tails!**");
        } else {
            message.channel.send("the dumbass who made this fucked something up");
        }
    }

    // Discogs NOT YET IMPLEMENTED
    if (command === "discogs") {
        var db = new Discogs().database();
        db.getRelease(3636, function(err, data) {
            message.channel.send(data.artists[0].name + " - " + "_" + data.title + "_");
            message.channel.send("Released in " + data.year);
            message.channel.send("Notes: " + data.notes);
        });
    }

    // Frinkiac
    if (command === "frinkiac" || command === "f") {
        if (args[0] === "-s") {
            var s = args.slice(1).join(" ");
            var first = true;
        } else {
            var s = args.slice(0).join(" ");
            var first = false;
        }
        frinkiac.search(s)
            .then(function(res) {
                if (res.status !== 200) {
                    throw res;
                } else {
                    return res.data;
                }
            })
            .catch(function(err) {
                throw err;
            })
            .then(function(data) {
                var memeURLs = data.map(frinkiac.memeMap, frinkiac);
                if (memeURLs.length === 0) {
                    message.channel.send("No results found for " + s);
                } else if (first) {
                    message.channel.send(memeURLs[0]);
                } else {
                    var rdmIdx = Math.floor(Math.random() * memeURLs.length);
                    message.channel.send(memeURLs[rdmIdx]);
                }
            });
    }

    // Random Classics of Game
    if (command === "cog") {
        if (!args[0]) {
            var rdmIdx = Math.floor(Math.random() * cog.cog.length);
            message.channel.send("https://youtube.com/watch?v=" + cog.cog[rdmIdx] + "?rel=0");
        } else {
            var idx = (args[0] - 1);
            if (idx <= cog.cog.length && idx > 0) {
                message.channel.send("https://youtube.com/watch?v=" + cog.cog[idx] + "?rel=0");
            } else {
                message.channel.send("please choose a number between 1 and " + cog.cog.length);
            }
        }
    }

});

client.login(config.token);
