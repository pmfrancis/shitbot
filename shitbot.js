const fs = require("fs");
const Discord = require("discord.js");
const { prefix, token } = require("./config.json");
const Discogs = require("disconnect").Client;

const client = new Discord.Client();
client.commands = new Discord.Collection();

const commandFiles = fs.readdirSync("./commands").filter(file => file.endsWith(".js"));

for (const file of commandFiles) {
    const command = require(`./commands/${file}`);
    client.commands.set(command.name, command);
}

client.on("ready", () => {
    client.user.setPresence({
        game: {
            name: 'with my ass',
            type: 'PLAYING'
        },
        status: 'available'
    })
    console.log("Running!");
});

client.on("message", (message) => {
    if (message.author.bot) return;
    if (message.content.indexOf(prefix) !== 0) return;

    //defining args
    const args = message.content.slice(prefix.length).split(/ +/);
    const commandName = args.shift().toLowerCase();

    const command = client.commands.get(commandName)
        || client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));

    if (!command) return;

    try {
        command.execute(message, args);
    } catch (error) {
        console.error(error);
        message.reply("There was an error trying to execute that command. Type `!help` to see all available commands!");
    }
});

client.login(token);
